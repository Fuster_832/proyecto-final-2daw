<?php
/*
Plugin Name: Plugin de funciones
Version: 1.0
Plugin URI: http://mestreacasa.gva.es/web/mariaenriquez
Description: Añade funcionalidades personalizadas a Wordpress
Author: David Fuster
Author URI: https://vps732099.ovh.net/wordpress
*/

//securización del plugin
defined('ABSPATH') or die;

/*
* Mensaje de error en login de WordPress
*/
function no_wordpress_errors(){
  return 'Te has equivocado introduciendo algun dato, vuelve a intentarlo';
}
add_filter( 'login_errors', 'no_wordpress_errors' );

/*
* Texto de bienvenida a los usuarios de wordpress
*/
function bienvenida_wordpress($wp_admin_bar){
  $my_account=$wp_admin_bar->get_node('my-account');
  $newtitle = str_replace('Hola', 'Bienvenido/a a PORTALEDU',$my_account->title);
  $wp_admin_bar->add_node(array(
      'id' => 'my-account',
      'title' => $newtitle,
  ));
}
add_filter( 'admin_bar_menu', 'bienvenida_wordpress',25);

/*
* Alerta via email cuando Google visita el blog
*/
if (strpos($_SERVER['HTTP_USER_AGENT'],'Googlebot') != false){
  $email_address = 'admin@vps732099.ovh.net';
  mail($email_address,'Alerta de Googlebot','Google ha visitado tu blog: '.$_SERVER['REQUEST_URI']);
}

/*
* Añade nuevos roles a Wordpress
*/
function add_roles(){
  add_role( 'direccion', 'Direccion', array(
    'read' => true,
    'delete_others_pages' => true,
    'delete_others_posts' => true,
    'delete_pages' => true,
    'delete_posts' => true,
    'delete_published_pages' => true,
    'delete_published_posts' => true,
    'edit_dashboard' => true,
    'edit_others_pages' => true,
    'edit_others_posts' => true,
    'edit_pages' => true,
    'edit_published_pages' => true,
    'edit_posts' => true,
    'remove_post' => true,
    'delete_published_posts' => true,
    'publish_pages' => true,
    'publish_posts' => true,
    'upload_files' => true,
    'edit_published_posts' => true,
    'customize' => true,
    'switch_themes' => true,
    'manage_categories' => true,
    'manage_links' => true,
    'manage_options' => true,
    'delete_private_pages' => true,
    'delete_private_posts' => true,
    'edit_private_pages' => true,
    'edit_private_posts' => true,
    'read_private_pages' => true,
    'read_private_posts' => true,
    'update_core' => true,
    'update_plugins' => true,
    'update_themes' => true,
    'install_plugins' => true,
    'install_themes' => true,
    'delete_themes' => true,
    'delete_plugins' => true,
    'edit_plugins' => true,
    'edit_themes' => true,
    'edit_files' => true,
    'edit_users' => true,
    'add_users' => true,
    'create_users' => true,
    'delete_users' => true,
    'list_users' => true,
    'edit_dashboard' => true,
    'activate_plugins' => true,
    'edit_theme_options' => true,
    'promote_users' => true
  ));

  add_role( 'mantenimiento', 'Mantenimiento', array(
    'read' => true,
    'delete_others_pages' => true,
    'delete_others_posts' => true,
    'delete_pages' => true,
    'delete_posts' => true,
    'delete_published_pages' => true,
    'delete_published_posts' => true,
    'edit_dashboard' => true,
    'edit_others_pages' => true,
    'edit_others_posts' => true,
    'edit_pages' => true,
    'edit_published_pages' => true,
    'edit_posts' => true,
    'remove_post' => true,
    'delete_published_posts' => true,
    'publish_pages' => true,
    'publish_posts' => true,
    'upload_files' => true,
    'edit_published_posts' => true,
    'edit_published_pages' => true,
    'customize' => true,
    'switch_themes' => true,
    'manage_categories' => true,
    'manage_links' => true,
    'manage_options' => true,
    'delete_private_pages' => true,
    'delete_private_posts' => true,
    'edit_private_pages' => true,
    'edit_private_posts' => true,
    'read_private_pages' => true,
    'read_private_posts' => true,
    'edit_dashboard' => true,
    'edit_themes' => true,
    'edit_theme_options' => true
  ));

  add_role( 'publicacion', 'Publicacion', array(
    'read' => true,
    'upload_files' => true,
    'publish_pages' => true,
    'publish_posts' => true,
    'edit_pages' => true,
    'edit_posts' => true,
    'delete_pages' => true,
    'delete_posts' => true,
    'delete_published_pages' => true,
    'delete_published_posts' => true,
    'edit_private_pages' => true,
    'edit_private_posts' => true,
    'read_private_pages' => true,
    'read_private_posts' => true,
    'edit_published_posts' => true,
    'edit_published_pages' => true,
  ));

  add_role( 'contenido', 'Contenido', array(
    'read' => true,
    'upload_files' => true,
    'edit_posts' => true,
    'delete_posts' => true,
    'edit_published_posts' => true,
    'delete_published_posts' => true,
  ));
}

/*
* Borra los roles creados en add_roles()
*/
function remove_roles(){
  remove_role( 'direccion' );
  remove_role( 'mantenimiento' );
  remove_role( 'publicacion' );
  remove_role( 'contenido' );
}

register_activation_hook( __FILE__, 'add_roles' );
register_deactivation_hook( __FILE__, 'remove_roles' );
